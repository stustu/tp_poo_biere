package DAO;

import modele.Atelier;

public interface AtelierDao extends Dao<Atelier> {
    public Atelier findFirstAvailable();
}
