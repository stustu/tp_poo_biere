package DAO;

import modele.Tache;

import java.util.Collection;

public interface TacheDao extends Dao<Tache> {
    public Collection<Tache> findAllNotScheduled();
}
