package test;

import DAO.AtelierDao;
import DAO.MachineDao;
import DAO.TacheDao;
import JPA.JpaAtelierDao;
import JPA.JpaMachineDao;
import JPA.JpaTacheDao;
import modele.Atelier;
import modele.Machine;
import modele.Tache;

import java.util.Date;

public class Test2 {
    public static void main(String[] args) {
        Atelier a = new Atelier();
        Machine m1 = new Machine(a);
        Machine m2 = new Machine(a);
        long timeMillis = System.currentTimeMillis();
        Tache t1 = new Tache(45, new Date(timeMillis+60000*120), 5);
        Tache t2 = new Tache(120, new Date(timeMillis+60000*150), 10);
        Tache t3 = new Tache(70, new Date(timeMillis+60000*180), 4);
        Tache t4 = new Tache(60, new Date(timeMillis+60000*300), 12);
        AtelierDao atelierManager = new JpaAtelierDao();
        MachineDao machineManager = new JpaMachineDao();
        TacheDao tacheManager = new JpaTacheDao();
        tacheManager.deleteAll();
        machineManager.deleteAll();
        atelierManager.deleteAll();
        a.addMachine(m1);
        a.addMachine(m2);
        atelierManager.create(a);
        tacheManager.create(t1);
        tacheManager.create(t2);
        tacheManager.create(t3);
        tacheManager.create(t4);
        // doit afficher les 4 taches
        System.out.println(t1);
        System.out.println(t2);
        System.out.println(t3);
        System.out.println(t4);
        m1.addTache(t1);
        m1.addTache(t2);
        m2.addTache(t3);
        m2.addTache(t4);
        tacheManager.update(t1);
        tacheManager.update(t2);
        tacheManager.update(t3);
        tacheManager.update(t4);
        System.out.println(m1.getTaches().toString());
        System.out.println(m2.getTaches().toString());
    }

}

