package test;

import modele.Atelier;
import modele.Machine;
import modele.Tache;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Test1 {
    public static void main(final String[] args) throws Exception {
        Atelier a = new Atelier();
        Machine m1 = new Machine(a);
        Machine m2 = new Machine(a);
        long timeMillis = System.currentTimeMillis();
        Tache t1 = new Tache(45, new Date(timeMillis+60000*120),5);
        Tache t2 = new Tache(120, new Date(timeMillis+60000*170),10);
        Tache t3 = new Tache(70, new Date(timeMillis+60000*200),4);
        Tache t4 = new Tache(60, new Date(timeMillis+60000*320),12);
        a.addMachine(m1);
        a.addMachine(m2);
        m1.addTache(t1);
        m1.addTache(t2);
        m2.addTache(t3);
        m2.addTache(t4);
        // Afficher l’atelier avec ses machines et les taches sur les machines
        // Verifier dates de début des taches
        // Verifier dates de disponibilité des machines
        // Verifier pénalité totale des machines
        System.out.println(a);
        a.getMachines().forEach((machine)->{
            System.out.println(machine);
            machine.getTaches().forEach((tache)->{
                System.out.println(tache.toString());
            });
        });
    }
}
