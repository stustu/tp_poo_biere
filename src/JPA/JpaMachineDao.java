package JPA;

import DAO.MachineDao;
import modele.Machine;
import org.hibernate.Session;

import java.util.Collection;

public class JpaMachineDao extends JpaDao<Machine> implements MachineDao {
    @Override
    public Session getSession() {
        return super.getSession();
    }

    @Override
    public boolean create(Machine obj) {
        return super.create(obj);
    }

    @Override
    public Machine find(Class c, Integer id) {
        return super.find(c, id);
    }

    @Override
    public Collection<Machine> findAll() {
        return super.findAll();
    }

    @Override
    public boolean update(Machine obj) {
        return super.update(obj);
    }

    @Override
    public boolean delete(Machine obj) {
        return super.delete(obj);
    }

    @Override
    public boolean deleteAll() {
        return super.deleteAll();
    }

    @Override
    public void close() {
        super.close();
    }
}
