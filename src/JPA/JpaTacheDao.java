package JPA;

import DAO.TacheDao;
import modele.Tache;
import org.hibernate.Session;

import java.util.Collection;

public class JpaTacheDao extends JpaDao<Tache> implements TacheDao {
    public JpaTacheDao() {
    }

    @Override
    public Collection<Tache> findAllNotScheduled() {
        return null;
    }

    @Override
    public Session getSession() {
        return super.getSession();
    }

    @Override
    public boolean create(Tache obj) {
        return super.create(obj);
    }

    @Override
    public Tache find(Class c, Integer id) {
        return super.find(c, id);
    }

    @Override
    public Collection<Tache> findAll() {
        return super.findAll();
    }

    @Override
    public boolean update(Tache obj) {
        return super.update(obj);
    }

    @Override
    public boolean delete(Tache obj) {
        return super.delete(obj);
    }

    @Override
    public boolean deleteAll() {
        return super.deleteAll();
    }

    @Override
    public void close() {
        super.close();
    }
}
