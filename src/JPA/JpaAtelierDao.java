package JPA;

import DAO.AtelierDao;
import modele.Atelier;
import org.hibernate.Session;

import java.util.Collection;

public class JpaAtelierDao extends JpaDao<Atelier> implements AtelierDao {

    @Override
    public Atelier findFirstAvailable() {
        return null;
    }

    @Override
    public Session getSession() {
        return super.getSession();
    }

    @Override
    public boolean create(Atelier obj) {
        return super.create(obj);
    }

    @Override
    public Atelier find(Class c, Integer id) {
        return super.find(c, id);
    }

    @Override
    public Collection<Atelier> findAll() {
        return super.findAll();
    }

    @Override
    public boolean update(Atelier obj) {
        return super.update(obj);
    }

    @Override
    public boolean delete(Atelier obj) {
        return super.delete(obj);
    }

    @Override
    public boolean deleteAll() {
        return super.deleteAll();
    }

    @Override
    public void close() {
        super.close();
    }
}
