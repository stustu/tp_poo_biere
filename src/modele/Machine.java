package modele;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "machine", schema = "tp_biere", catalog = "")
public class Machine {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false,insertable = false,updatable = false)
    private int idMachine;

    @Basic
    @Column(name = "DATEDISPO")
    private Date dateDispo;

    @Basic
    @Column(name = "PENALITETOTALE")
    private Double penaliteTotale;

    @ManyToOne
    @JoinColumn(name="NATELIER",referencedColumnName = "ID",nullable = true)
    private Atelier nAtelier;

    @OneToMany(mappedBy="NMACHINE", cascade = {CascadeType.ALL})
    private List<Tache> taches = new ArrayList<Tache>();

    public Machine(Atelier a){
        this.penaliteTotale = 0.0;
        this.dateDispo = new Date();
        this.nAtelier = a;
    }

    /**
     * Ajout d'une tâche
     * */
    public boolean addTache(Tache t)
    {
        // Mise à jour de la machine de la tache et de la date de dispo
        t.setNMACHINE(this);
        if (this.getDateDispo() != null)
            t.setDateDebut(this.getDateDispo());


        LocalDateTime dispo = LocalDateTime.ofInstant(this.getDateDispo().toInstant(), ZoneId.systemDefault());
        dispo = dispo.plusMinutes((long)t.getTempsProd());

        Date datedispo = Date.from(dispo.toInstant(ZoneOffset.UTC));
        this.setDateDispo(datedispo);


        long nbMinutesRetard = 0;
        if ( !t.getDateLimite().before(datedispo) )
        {
            System.out.println("pas de pénalités pour" + t.getIdTache());
            // Il n'y aura pas de pénalités
        }
        else
        {
            System.out.println("pénalités pour" + t.getIdTache());
            LocalDateTime Delai = Instant.ofEpochMilli(t.getDateLimite().getTime()).atZone(ZoneOffset.UTC).toLocalDateTime();
            LocalDateTime Reel = Instant.ofEpochMilli(datedispo.getTime()).atZone(ZoneOffset.UTC).toLocalDateTime();
            //Calcul de la durée entre la fin souhaitée de la tache et la fin prévue sur la machine
            Duration dur = Duration.between(Reel, Delai);
            nbMinutesRetard = dur.toMinutes();
            this.setPenaliteTotale(this.getPenaliteTotale() + (t.getPenaliteRetard() * nbMinutesRetard));
        }

        Date dispoAtelier = this.getnAtelier().getDateDispo();
        if (dispoAtelier != null)
        {
            if (dispoAtelier.before(datedispo))
            {
                this.getnAtelier().setDateDispo(datedispo);
            }
        }
        else
            this.getnAtelier().setDateDispo(datedispo);

        this.taches.add(t);

        return true;
    }

    public int getIdMachine() {
        return idMachine;
    }

    public void setIdMachine(int idMachine) {
        this.idMachine = idMachine;
    }

    public Date getDateDispo() {
        return dateDispo;
    }

    public void setDateDispo(Date dateDispo) {
        this.dateDispo = dateDispo;
    }

    public Double getPenaliteTotale() {
        return penaliteTotale;
    }

    public void setPenaliteTotale(Double penaliteTotale) {
        this.penaliteTotale = penaliteTotale;
    }

    public Atelier getnAtelier() {
        return nAtelier;
    }

    public void setnAtelier(Atelier nAtelier) {
        this.nAtelier = nAtelier;
    }

    public List<Tache> getTaches() {
        return taches;
    }

    public void setTaches(List<Tache> taches) {
        this.taches = taches;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Machine machine = (Machine) o;
        return idMachine == machine.idMachine &&
                Objects.equals(dateDispo, machine.dateDispo) &&
                Objects.equals(penaliteTotale, machine.penaliteTotale) &&
                Objects.equals(nAtelier, machine.nAtelier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMachine, dateDispo, penaliteTotale, nAtelier);
    }

    @Override
    public String toString() {
        return "Machine{" +
                "idMachine=" + idMachine +
                ", dateDispo=" + dateDispo +
                ", penaliteTotale=" + penaliteTotale +
                ", nAtelier=" + nAtelier +
                " taches= " +
                '}';
    }
}
