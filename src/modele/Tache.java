package modele;

import sun.plugin2.os.windows.FLASHWINFO;

import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "tache", schema = "tp_biere", catalog = "")
public class Tache {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false,insertable = false,updatable = false)
    private int idTache;

    @Basic
    @Column(name = "TEMPSPROD")
    private long tempsProd;

    @Basic
    @Column(name = "DATEDEBUT")
    private Date dateDebut;

    @Basic
    @Column(name = "DATELIMITE")
    private Date dateLimite;

    @Basic
    @Column(name = "PENALITERETARD")
    private Double penaliteRetard;

    @ManyToOne
    @JoinColumn(name="NMACHINE",referencedColumnName = "ID",nullable = true)
    private Machine NMACHINE;

    public Tache(int idTache, Date dateLimite, long tempsProd) {
        this.idTache = idTache;
        this.tempsProd = tempsProd;
        this.dateDebut = new Date();
        this.dateLimite = dateLimite;
        this.penaliteRetard = 5.0;
        this.NMACHINE = null;
    }

    public int getIdTache() {
        return idTache;
    }

    public void setIdTache(int idTache) {
        this.idTache = idTache;
    }

    public long getTempsProd() {
        return tempsProd;
    }

    public void setTempsProd(long tempsProd) {
        this.tempsProd = tempsProd;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateLimite() {
        return dateLimite;
    }

    public void setDateLimite(Date dateLimite) {
        this.dateLimite = dateLimite;
    }

    public Double getPenaliteRetard() {
        return penaliteRetard;
    }

    public void setPenaliteRetard(Double penaliteRetard) {
        this.penaliteRetard = penaliteRetard;
    }

    public Machine getNMACHINE() {
        return NMACHINE;
    }

    public void setNMACHINE(Machine NMACHINE) {
        this.NMACHINE = NMACHINE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tache tache = (Tache) o;
        return idTache == tache.idTache &&
                Objects.equals(tempsProd, tache.tempsProd) &&
                Objects.equals(dateDebut, tache.dateDebut) &&
                Objects.equals(dateLimite, tache.dateLimite) &&
                Objects.equals(penaliteRetard, tache.penaliteRetard) &&
                Objects.equals(NMACHINE, tache.NMACHINE);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTache, tempsProd, dateDebut, dateLimite, penaliteRetard, NMACHINE);
    }

    @Override
    public String toString() {
        return "Tache{" +
                "idTache=" + idTache +
                ", tempsProd=" + tempsProd +
                ", dateDebut=" + dateDebut +
                ", dateLimite=" + dateLimite +
                ", penaliteRetard=" + penaliteRetard +
                ", NMACHINE=" + NMACHINE +
                '}';
    }
}
