package modele;

import javax.crypto.Mac;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "atelier", schema = "tp_biere", catalog = "")
public class Atelier {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false,insertable = false,updatable = false)
    private int idAtelier;

    @Basic
    @Column(name = "DATEDISPO")
    private Date dateDispo;

    @OneToMany(mappedBy="nAtelier", cascade = {CascadeType.ALL})
    private List<Machine> machines;

    public Atelier(){
        this.dateDispo = new Date();
        this.machines = new ArrayList<Machine>();
    }

    public boolean addMachine(Machine m){
        if(!this.machines.add(m)) return false;
        return true;
    }

    public int getIdAtelier() {
        return idAtelier;
    }

    public void setIdAtelier(int idAtelier) {
        this.idAtelier = idAtelier;
    }

    public Date getDateDispo() {
        return dateDispo;
    }

    public void setDateDispo(Date dateDispo) {
        this.dateDispo = dateDispo;
    }

    public List<Machine> getMachines() {
        return machines;
    }

    public void setMachines(List<Machine> machines) {
        this.machines = machines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Atelier atelier = (Atelier) o;
        return idAtelier == atelier.idAtelier &&
                Objects.equals(dateDispo, atelier.dateDispo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAtelier, dateDispo);
    }

    @Override
    public String toString() {
        return "Atelier{" +
                "idAtelier=" + idAtelier +
                ", dateDispo=" + dateDispo +
                ", machines= " +
                '}';
    }

}
